package hello

import "testing"

func TestHello(t *testing.T) {
	if m := Hello(); m != "Hello World" {
		t.Errorf("expect Hello World, got %s", m)
	}
}
